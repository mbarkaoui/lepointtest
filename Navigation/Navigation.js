import React from 'react';
import {StyleSheet,Image,Button,Text } from 'react-native'

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs'

import Home from '../Components/Home'
import PersonneDetail from '../Components/PersonneDetail'
import Favorites from '../Components/Favorites'

 import ActeursScreen from '../Components/Rubriques/Acteurs'
 import DirecteursScreen from '../Components/Rubriques/Directeurs'
 import ProducteursScreen from '../Components/Rubriques/Producteurs'

const PersonsTopViewPagerNavigator = createMaterialTopTabNavigator(  
    {  
        Acteurs: { 
            screen:ActeursScreen,
            navigationOptions: {
                title: "Acteurs"
            }
            },
        Directeurs: { 
            screen:DirecteursScreen,
            navigationOptions: {
                title: "Directeurs"
            }
            },  
        Producteurs: { 
            screen:ProducteursScreen,
            navigationOptions: {
                title: "Producteurs"
            }
            },  
    },  
    {  
        tabBarOptions: {  
            activeTintColor: 'white',  
            showIcon: false,  
            showLabel:true,  
            labelStyle: {
                fontFamily: 'American Typewriter',
                fontSize: 12,
                fontWeight: 'bold',
                justifyContent :'center',
                paddingTop : 40
              },            
            style: {  
                height:90,
                backgroundColor:'red',
            }  
        },  
    }  
) 

const ActeursStackNavigator = createStackNavigator(
    {
    Acteurs: {
        screen: ActeursScreen,
        navigationOptions: {
            title: "Acteurs"
        }
        },
    PersonneDetail: {
            screen: PersonneDetail,
    },
    },
    {
      headerMode: 'none',
    }
);

const DirecteursStackNavigator = createStackNavigator(
    {
    Directeurs: {
        screen: DirecteursScreen,
        navigationOptions: {
            title: "Directeurs"
        }
        },
    PersonneDetail: {
            screen: PersonneDetail,
    },
    },
    {
      mode: 'modal',
      headerMode: 'none',
    }
);

const ProducteursStackNavigator = createStackNavigator(
    {
    Producteurs: {
        screen: ProducteursScreen,
        navigationOptions: {
            title: "Producteurs"
        }
        },
    PersonneDetail: {
            screen: PersonneDetail,
               navigationOptions: {
            title: "Producteurs"
        }
    },
    },
    {
      headerMode: 'none',
    }
);

const HomeStackNavigator = createStackNavigator(
    {
    Home: {
        screen: Home,
        navigationOptions: {  
            headerLeft:() => (
                <Text style={styles.title_text}
                >LePoint</Text>
              ),
            title: "",  
            headerStyle: {
                backgroundColor: 'red',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
        }
        },
    PersonneDetail: {
        screen: PersonneDetail,
        navigationOptions: {
            title: "Detail",
            headerStyle: {
                backgroundColor: 'red',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
        }
    }
    }
);

const FavorisStackNavigator = createStackNavigator({
    Favorites: {
        screen: Favorites,
        navigationOptions: {
            title: "Favoris",
            headerStyle: {
                backgroundColor: 'red',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
        } 
    },
    PersonneDetail: {
        screen: PersonneDetail,
        navigationOptions: {
            title: "Details",
            headerStyle: {
                backgroundColor: 'red',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
        }
        
    },
    });

const PersonStackNavigator = createStackNavigator({
    Persons: {
        screen: PersonsTopViewPagerNavigator,
        navigationOptions: {
            title: "Favoris",
        }
    },
    PersonneDetail: {
        screen: PersonneDetail,
        navigationOptions: {
            title: "Details",
        }
    },
    },   
    {
    mode: 'modal',
    headerMode: 'none',
});

const middleNavigator = createStackNavigator({
    Persons: {
        screen: PersonsTopViewPagerNavigator,
        navigationOptions: {
            headerShown: false
        }
    },
    PersonneDetail: {
        screen: PersonneDetail,
        navigationOptions: {
            title: "Detail",
            headerStyle: {
                backgroundColor: 'red',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
        }
        
    },
    },{
        headerMode: 'float',
    });

const MoviesTabNavigator = createBottomTabNavigator({
    Home: {
        screen: HomeStackNavigator,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                <Image
                    source={require('../Images/ic_home.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                  />
              ),
        }
    },
   
    Persons: {
        screen: middleNavigator,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                <Image
                    source={require('../Images/ic_person.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                  />
              ),
            }
    },
    Favorites: {
        screen: FavorisStackNavigator,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                <Image
                    source={require('../Images/favorite_border.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                  />
              ),
            }
        }
},{
        tabBarOptions: {
            showIcon: true,
            activeTintColor: 'red', // active icon color
            inactiveTintColor: 'black',
            style: {
                height:50
            }
        }
})


const styles = StyleSheet.create({
    icon: {
        width:30,
        height: 30
    } ,title_text: {
        fontWeight: 'bold',
        fontSize: 20,
        color:"white",
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 30,
        fontFamily: 'American Typewriter',
        marginTop : 10,
      }
})
export default createAppContainer(MoviesTabNavigator)