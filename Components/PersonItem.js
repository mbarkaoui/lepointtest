// Components/PersonItem.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import  { getImageFromApi } from '../API/MOVIEApi'
import {Asset} from 'expo-asset';


class PersonItem extends React.Component {

    constructor(props){
      super(props);
      this.person = this.props.person
      this.state = {
      image: getImageFromApi(this.person.img)

    }
  }

    componentDidMount() {
      this.setState({
        image: getImageFromApi(this.person.img)
      })
    }

    loadFallback(){
      const imageURI = Asset.fromModule(require('../Images/placeholder.jpeg')).uri;

      this.setState({image: imageURI })
    }

    _displayFavoriteImage() {
        if (this.props.isPersonFavorite) {
          return (
            <Image
              style={styles.favorite_image}
              source={require('../Images/favorite.png')}
            />
          )
        }
      }

  render() {
    const {person,displayDetailForPerson} = this.props
    return (
      <TouchableOpacity
      onPress={() => displayDetailForPerson(person.id,person)} style={styles.main_container}>
        <Image
          style={styles.image}
          source={{uri: this.state.image}}
          onError={()=>this.loadFallback()}
        />
        <View style={styles.content_container}>
          <View style={styles.title_container}>
          {this._displayFavoriteImage()}
           <Text style={styles.profession_text}>{person.fullname}</Text> 
          </View>
          <View style={styles.header_container}>
           <Text style={styles.title_text} numberOfLines={2}>{person.profession}</Text>

          </View>

          <View style={styles.description_container}>
            <Text style={styles.description_text} numberOfLines={6}>{person.commentaire}</Text>
            {/* La propriété numberOfLines permet de couper un texte si celui-ci est trop long, il suffit de définir un nombre maximum de ligne */}
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date_text}>Né le {person.date_naissance}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 210,
    flexDirection: 'row'
  },
  image: {
    width: 120,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  title_container: {
    flex: 4,
    flexDirection: 'row'
  },
  header_container: {
    flex: 4,
    flexDirection: 'column'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  profession_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#666666'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666'
  },
  date_container: {
    flex: 1
  },
  date_text: {
    textAlign: 'right',
    fontSize: 14
  },
  favorite_image:{
        tintColor:'red',
        width: 30,
        height: 30,
        marginRight: 5
      }
})

export default PersonItem