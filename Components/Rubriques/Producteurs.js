// Components/ProducteursScreen.js

import React from 'react'
import { StyleSheet, View, TextInput, Button, Text, FlatList, ActivityIndicator} from 'react-native'
import { getProducteursFromApi, getImagePersonFromDetail, getPersonDetailFromApi } from '../../API/MOVIEApi'
import PersonsList from '../../Components/PersonsList'

class ProducteursScreen extends React.Component {

  constructor(props) {
    super(props)
    this.offset = 0
    this.state = {
      persons: [],
      isLoading: false    }

    this._loadPersons = this._loadPersons.bind(this)

  }

  componentDidMount() {
    this._loadPersons()
  }

  _displayLoading() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  _loadPersonDetail(idPerson) {
    getPersonDetailFromApi(idPerson).then(data => {
    })
  }

  _loadPersons() {
    this.setState({ isLoading: true })
    getProducteursFromApi(20,this.offset).then(data => {
      this.offset = data.offset
      this.setState({
        persons: [...this.state.persons, ...data.persons],
        isLoading: false
      })
    })
  }

  render() {

    return (
      <View style={styles.main_container}>
        <PersonsList
          navigation={this.props.navigation} 
          persons={this.state.persons}
          navigation={this.props.navigation}
          loadPersons={this._loadPersons}
          offset={this.offset}
       />
        {this._displayLoading()}
      </View>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  modalView: {
    flex: 1,
    marginTop: 22,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default ProducteursScreen