// Components/PersonsList.js

import React from 'react'
import { StyleSheet, FlatList,View,Text,Pressable,Alert} from 'react-native'
import PersonItem from './PersonItem'
import {getPersonDetailFromApi} from '../API/MOVIEApi'
import { connect } from 'react-redux'

function HandleError() {
   Alert.alert(
      "Erreur Serveur",
      "Un probléme serveur est survenue, Veuillez réessayer plus tard",
      [
        {
          text: "Annuler",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ]
    );
}

class PersonsList extends React.Component {

  constructor(props) {
    super(props)
    this.props = props
    this.fromFavorites = this.props.fromFavorites
    this.state = {
      persons: []
    }
  }

  _displayDetailForPerson = (idPerson,personItem) => {

  getPersonDetailFromApi(idPerson).then(data => {
  this.props.navigation.navigate('PersonneDetail', {person: data,item:personItem})

  }, function (reason) {
    HandleError()
     }
    ).catch(function () {
      HandleError()
 });
}

  render() {
    return (
      <View style={styles.list}>
        <FlatList
          style={styles.list}
          data={this.fromFavorites? this.props.favoritesPerson : this.props.persons}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <PersonItem
              person={item}
              isPersonFavorite={(this.props.favoritesPerson.findIndex(person => person.id === item.id) !== -1) ? true : false}
              displayDetailForPerson={this._displayDetailForPerson}
            />
          )}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            if (typeof this.props.offset !== 'undefined') {
              this.props.loadPersons()
            }
          }}
        />
        </View>
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1
  },
  modalView: {
    flex: 1,
    marginTop: 22,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    }
  }
})

const mapStateToProps = state => {
  return {
    favoritesPerson: state.favoritesPerson
  }
}

export default connect(mapStateToProps)(PersonsList)