const initateState = {favoritesPerson: []}

function toggleFavorite(state=initateState,action) {
    let nextState
    switch (action.type) {
        case 'TOGGLE_FAVORITE':
        const favoritePersonIndex = state.favoritesPerson.findIndex(item => item.id === action.value.id)
        if (favoritePersonIndex !== -1) {

            nextState = { ...state, 
                favoritesPerson:state.favoritesPerson.filter((item,index) => index !== favoritePersonIndex)}
            //suppression
        } else {
            nextState = { ...state,
            favoritesPerson:[...state.favoritesPerson, action.value]}
        }
          return nextState || state  
    
        default:
            return state
    }
}

export default toggleFavorite