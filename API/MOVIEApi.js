const baseURL = "https://ba-api.lpnt.fr/"

export async function getHomeItemsFromApi(limit,offset) {
    const url = baseURL + 'rubrique/home/limit/' + limit + '/offset/' + offset
    return fetch(url)
    .then((response) => response.json())
    .catch((error) => {console.log(error)
    return null})
}

export async function getActeursFromApi(limit,offset) {
    const url = baseURL +'rubrique/acteurs/limit/' + limit + '/offset/' + offset
    return fetch(url)
    .then((response) => response.json())
    .catch((error) => {console.log(error)
    return null})
}

export async function getProducteursFromApi(limit,offset) {
    const url = baseURL + 'rubrique/producteurs/limit/' + limit + '/offset/' + offset
    return fetch(url)
    .then((response) => response.json())
    .catch((error) => {console.log(error)
    return null})
}

export async function getDirecteursFromApi(limit,offset) {
    const url = baseURL + 'rubrique/directeurs/limit/' + limit + '/offset/' + offset
    return fetch(url)
    .then((response) => response.json())
    .catch((error) => {console.log(error)
    return null})
}

export function getImageFromApi(personPhoto) {
    return baseURL + 'images/personne/' + personPhoto
}
export async function getPersonDetailFromApi(idPerson) {
    const url = 'https://ba-api.lpnt.fr/personne/' + idPerson
    return new Promise(
        function (resolve, reject) {
     fetch(url,
        { method: 'get',
    headers: {
      'Accept': 'application/json, text/plain, */*', 
      'Content-Type': 'application/json'
    }  })
    .then(function(response) {
        if (response.ok) {
            resolve(response.json())
        } else {
        reject(new Error(`Unable to retrieve events.\nInvalid response received - (${response.status}).`))
        }
      })
    .catch((error) => {
        reject(new Error(`Unable to retrieve events.\n${error.message}`));
    }) 
}
    );
}