// Components/PersonneDetail.js

import React from 'react'
import { StyleSheet, View, Text, ActivityIndicator, ScrollView ,Image,Button, TouchableOpacity,Asset} from 'react-native'
import { getPersonDetailFromApi ,getImageFromApi}  from '../API/MOVIEApi'

import { connect } from 'react-redux'
import moment from 'moment'


class PersonneDetail extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            image:undefined,
            person:undefined,
            item:undefined,
            isLoading:true
        }
    }

    componentDidMount(){
         this.setState({
        image:getImageFromApi(this.props.navigation.getParam('person').content.photo),
        person: this.props.navigation.getParam('person'),
        item:this.props.navigation.getParam('item'),
        isLoading: false
    })
    }

    _displayFavoriteImage() {
      var sourceImage = require('../Images/favorite_border.png')
      if (this.props.favoritesPerson.findIndex(item => item.id === this.state.item.id) !== -1) {
        sourceImage = require('../Images/favorite.png')
      }
      return (
        <EnlargeShrink
          shouldEnlarge={shouldEnlarge}>
          <Image
            style={styles.favorite_image}
            source={sourceImage}
          />
        </EnlargeShrink>
      )
    }
  

    _displayFavoriteImage() {
        var sourceImage = require('../Images/favorite_border.png')
        if (this.props.favoritesPerson.findIndex(item => item.id === this.state.item.id) !== -1) {
            var sourceImage = require('../Images/favorite.png')
        }

        return (
            <Image 
            source = {sourceImage}
            style = {styles.favorite_image}/>
        )
    }

    loadFallback(){
      const imageURI = Asset.fromModule(require('../Images/favorite.png')).uri;

      this.setState({image: imageURI })
    }


    _displayPerson() {
        const { person } = this.state
        if (person != undefined) {
          return (
            <ScrollView style={styles.scrollview_container}>
            
              <Text style={styles.title_text}>{person.content.nom}</Text>
              <Text style={styles.profession_text}>{person.content.profession}</Text>

              <Text style={styles.center_text}>Né le{moment(new Date(person.content.date_naissance)).format('DD/MM/YYYY')}</Text>
              <View style={{ flex: 1 ,alignItems:'center'}}>

              <Image
          style={styles.image}
          source={{uri: this.state.image}}
          onError={()=>this.loadFallback()}
        />
              </View>


              <TouchableOpacity style={styles.favorite_container} onPress={() => this._toggleFavorite()}>
              {this._displayFavoriteImage()}
              </TouchableOpacity>
              <Text style={styles.description_text}>{person.content.commentaire}</Text>
        
            </ScrollView>
          )
        }
      }

    _displayingLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large'/>
                </View>
            )
        }
    }
    
    _toggleFavorite(){
      const action = {type: "TOGGLE_FAVORITE", value:this.props.navigation.getParam('item')}
      this.props.dispatch(action)
    }

  render() {
      const idPerson= this.props.navigation.getParam('idPerson');

    return (
      <View style={styles.main_container}>
        {this._displayPerson()}
        {this._displayingLoading()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    margin:20
  },
  loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollView_container: {
        flex:1,
        alignContent: 'center',

    },
    image: {
      width: 150,
      height: 180,
      alignItems: 'center'

    },
    profession_text: {
      fontWeight: 'bold',
      fontSize: 20,
      flex: 1,
      flexWrap: 'wrap',
      marginLeft: 5,
      marginRight: 5,
      marginTop: 10,
      marginBottom: 10,
      color: 'gray',
      textAlign: 'center'
    },
      title_text: {
        fontWeight: 'bold',
        fontSize: 35,
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        color: '#000000',
        textAlign: 'center'
      },
      description_text: {
        fontStyle: 'italic',
        color: '#666666',
        margin: 5,
        marginBottom: 15
      },
      favorite_container: {
        color:'red',
        margin:10,
        alignItems: 'center', // Alignement des components enfants sur l'axe secondaire, X ici
    },
      favorite_image: {
        tintColor:'red',
        width: 40,
        height: 40
      },
      center_text: {
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 5,
        marginTop: 20,
        marginBottom: 20

      },
      default_text: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
      }
})

  const mapStateToProps = (state) => {
    return {
      favoritesPerson: state.favoritesPerson
    }
  }

  export default connect(mapStateToProps)(PersonneDetail)